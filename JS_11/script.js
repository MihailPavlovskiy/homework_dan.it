const eyeIcon = Array.from(document.querySelectorAll('.icon-password'));
const input = document.querySelectorAll('.input');
const btn = document.querySelector('.btn')

eyeIcon.forEach(e=>{
    e.addEventListener('click', ()=>{
        if (e.classList.contains('fa-eye')){
            e.classList.replace('fa-eye', 'fa-eye-slash');
            input[eyeIcon.indexOf(e)].setAttribute('type', 'text')
        }else {
            e.classList.replace('fa-eye-slash', 'fa-eye');
            input[eyeIcon.indexOf(e)].setAttribute('type', 'password')

        }
    })
});
btn.addEventListener('click', (e)=>{
    e.preventDefault()
    if (input[0].value === input[1].value){
        alert('You are welcome')
    }else {
        alert('Потрібно ввести однакові значення');
        input[1].style.backgroundColor = 'red'
    }
})