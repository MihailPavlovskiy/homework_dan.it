// 1.Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.
// 1. Конструккція допомагає оброблювати помилки. Наприклад, якщо у користувача, щось пішло не так, то ми стилізуємо помилку та підганяємо під ситуацію, щоб користувач зрозумів у чому помилка.

const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];


const root = document.getElementById('root')
const ul = document.createElement('ul')
root.append(ul)

const createList = (list) =>{

        list.forEach((elem,i)=>{
            try {
                if (elem.name && elem.author && elem.price){
                    let li = document.createElement('li')
                    ul.append(li)
                    li.append(`Author: ${elem.author}, Book: ${elem.name}, Price: ${elem.price} $`)
                }
                if (!elem.price){
                    throw new Error(`Book: ${i+1} - don't have price`)
                }else if (!elem.author){
                    throw new Error(`Book: ${i+1} - don't have author`)
                }else if (!elem.name){
                    throw new Error(`Book: ${i+1} - don't have book name`)
                }
            }catch (error){
                console.log(error.name, error.message)
            }
        })



}
createList(books)

