// Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.
// Технологія, що дозволяє взаємодіяти з сервером без перезавантаження сторінки, що відображається в браузері.
// AJAX дозволяє відправити запит на сервер і отримати відповідь у фоновому режимі.


const root = document.getElementById('root')
const ul = document.createElement('ul')
root.append(ul)

 async function getAllFilms (url){
    const req = await fetch(`${url}`)
    const response = await req.json()
    await response.forEach(e => {
        const li = document.createElement('li')
        console.log(e.name)
         ul.append(li)
         li.innerText = `The Film: ${e.name}, Episode: ${e.episodeId}, Description: ${e.openingCrawl} `
        const findChar = async (persons) => {
            for await (let elem of persons){
                const charRes = await  fetch(`${elem}`)
                const char = await  charRes.json()
                const h6 =  document.createElement('h6')
                h6.innerText = `Character: ${char.name}`
                li.append(h6)
            }
        }
    findChar(e.characters)
    })
}
void getAllFilms('https://ajax.test-danit.com/api/swapi/films')

