function addArr (arr, parent = document.body){

    const ul = document.createElement('ul')
    const div = document.createElement('div')
    for (let i = 0; i<arr.length; i++ ){
        const li = document.createElement('li')
        ul.appendChild(li)
        li.innerText = arr[i]
    }
    parent.append(div)
    div.appendChild(ul)

}
const arr = ["1", "2", "3", "sea", "user", 23]
addArr(arr)
