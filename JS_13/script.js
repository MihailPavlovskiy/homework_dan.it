const image = Array.from(document.querySelectorAll('.image-to-show'))
const btnStop = document.createElement('button');
btnStop.innerText = 'Stop';
const btnCont = document.createElement('button')
btnCont.innerText = 'Continue';
let i = 0;
let delay = 3000;
let delayBtn = 1500;

function addBtn(){
    document.body.append(btnStop);
    document.body.append(btnCont);
}
function showImg (){
        if (i===image.length - 1 ){
            image[i].style.display = 'none';
            i = 0
            image[0].style.display = 'block';
        }else {
            image[i].style.display = 'none';
            image[i + 1].style.display = 'block';
            i++
        }
}
btnStop.addEventListener('click', function () {
    clearInterval(timerId)
})
btnCont.addEventListener('click', function (){
     setInterval(showImg, delay);
})
let timerId = setInterval(showImg, delay);
setTimeout(addBtn, delayBtn);


/*
1.Опишіть своїми словами різницю між функціями setTimeout() і setInterval()`.
1.setTimeout()-дає можливість викликати функцію через деякий час. setInterval()-дфє можливість викликати функцію регулярно з якимось проміжком часу
2.Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?
2.Буде пріорітетність у виконанні. Функція виконається як умога швидше, але не раніше, коду, що знаходиться перед ним.
3.Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?
3.Щоб певний код далі не працював та не створював зайві дії. Тоді сторінка та дії на ній будуть швидші.

*/



<div class="block-staff-bg">
    <div class="container">
        <div class="block-staff-main-text">
            <h2 class="block-staff-text">What People Say About theHam</h2>
            <span class="smallLine-1"></span>
            <span class="smallLine-2 line-retreat-works-block"></span>
            <img class="block-staff-commas" src="img/commas.svg" alt="commas">

            <div class="block-staff-catalog">
                <div class="block-staff-catalog-employers">
                    <div class="employee-info-1 employee-info">
                        <p class="employee-description">Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.
                            Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.</p>
                        <p class="employee-name">Hasan Ali</p>
                        <p class="employee-job">UX Designer</p>
                        <img class="employee-bigPhoto cover-photo" src="Lora.png" alt="Hasan Ali">
                    </div>
                    <div class="employee-info-2 employee-info">
                        <p class="employee-description">Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.
                            Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.</p>
                        <p class="employee-name">Hasan Ali</p>
                        <p class="employee-job">UX Designer</p>
                        <img class="employee-bigPhoto cover-photo" src="David.png" alt="Hasan Ali">
                    </div>
                    <div class="employee-info-3 employee-info active">
                        <p class="employee-description">Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.
                            Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.</p>
                        <p class="employee-name">Hasan Ali</p>
                        <p class="employee-job">UX Designer</p>
                        <img class="employee-bigPhoto" src="Hasan+.png" alt="Hasan Ali">
                    </div>
                    <div class="employee-info-4 employee-info">
                        <p class="employee-description">Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.
                            Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.</p>
                        <p class="employee-name">Hasan Ali</p>
                        <p class="employee-job">UX Designer</p>
                        <img class="employee-bigPhoto cover-photo" src="Victoria.png" alt="Hasan Ali">
                    </div>
                </div>
                <div class="block-staff-catalog-roulette">
                    <div class="btn-arrow btn-left"><img src="perv-arr.svg" alt="left"></div>
                    <img class="employee-photo" src="Lora.png" alt="Svetlana">
                    <img class="employee-photo" src="David.png" alt="Max">
                    <img class="employee-photo upContent" src="Hasan.png" alt="Hasan Ali">
                    <img class="employee-photo" src="Victoria.png" alt="Liza">
                    <div class="btn-arrow btn-right"><img src="next-arr.svg" alt="right"></div>
                </div>
            </div>
        </div>
    </div>
</div>