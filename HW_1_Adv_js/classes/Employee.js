class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
    get name (){
        return this._name
    }
    set name (name){
        return this._name = name
    }
    get age (){
        return this._age
    }
    set age (age){
        return this._age = age
    }
    get salary (){
        return this._salary
    }
    set salary (salary){
        return this._salary = salary
    }
}
