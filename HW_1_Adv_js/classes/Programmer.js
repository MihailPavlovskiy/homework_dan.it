class Programmer extends Employee{
    constructor(name, age, salary, lang) {
        super(name, age,salary);
        this._lang = lang
    }
    get lang (){
        return this._lang
    }
    set lang(lang){
        return this._lang = lang
    }
    get salary() {
        return this._salary;
    }
    set salary(salary) {
        return  this._salary = salary * 3;
    }

}

