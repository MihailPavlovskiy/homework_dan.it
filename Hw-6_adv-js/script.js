const btnSearch = document.getElementById('search-btn')
const root = document.getElementById('root')

btnSearch.addEventListener('click', async function () {
    const getResIp = await fetch(`https://api.ipify.org/?format=json`)
    const getIp = await getResIp.json()
    const ipAddress = getIp.ip
    console.log(ipAddress)
    const sendResIp = await fetch(`http://ip-api.com/json/${ipAddress}?fields=continent,country,region,city,district`)
    const resultIp = await sendResIp.json()
    const {continent, country, region, city, district} = resultIp
    root.innerHTML = `
    <div>Continent: ${continent}</div>
    <div>Country: ${country}</div>
    <div>Region: ${region}</div>
    <div>City: ${city}</div>
    <div>District: ${district}</div>
    `
    console.log(resultIp)
})





