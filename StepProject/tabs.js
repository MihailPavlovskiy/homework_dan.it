const tabsItem = document.querySelectorAll('.tabs-item');
const tabsContent = document.querySelectorAll('.tab-item');
tabsItem.forEach(function (item){
    item.addEventListener('click', function (){
        let currentTitle = item;
        let tabID =  currentTitle.getAttribute('data-tab');
        let currentContent = document.querySelector(tabID)
        tabsItem.forEach(function (item){
            item.classList.remove('active')
        })
        tabsContent.forEach(function (item){
            item.classList.remove('active')
        })
        currentTitle.classList.add('active')
        currentContent.classList.add('active')

    })
})


