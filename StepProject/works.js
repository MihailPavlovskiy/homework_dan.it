const list = document.querySelector('.works-list-items'),
    items = [...document.querySelectorAll('.catalog-item')],
    listItems = document.querySelectorAll('.works-list-item')

function filter() {
    list.addEventListener('click', event =>{
        const targetFilter = event.target.dataset.filter;
        const targetFilterItem = event.target;

        listItems.forEach(item =>{
            item.classList.remove('active');
        });
        targetFilterItem.classList.add('active');

        switch (targetFilter){
            case 'all':
                getItems('catalog-item')
                break
            case 'graphic':
                getItems(targetFilter)
                break
            case 'web':
                getItems(targetFilter)
                break
            case 'landing':
                getItems(targetFilter)
                break
            case 'wordpress':
                getItems(targetFilter)
                break
        }
    });
}

filter();

function getItems(className) {
    items.forEach(item=>{
        item.classList.add('hide')
    });

    items.forEach(item => {
        if (item.classList.contains(className)){
            item.classList.remove('hide');
        }
    })
}


const loadMore = document.getElementById('load');
let currentItems = 12;
const plus = document.getElementById('plus')
loadMore.addEventListener('click', (event) => {
    const elementList = [...document.querySelectorAll('.photos-list .allItems')];

    for (let i = currentItems; i < currentItems + 12; i++) {
        if (elementList[i]) {
            elementList[i].classList.remove('visible');
        }
    }
    currentItems += 12;

    if (currentItems >= elementList.length) {
        event.target.style.display = 'none';
        plus.style.display = 'none'
    }
})
