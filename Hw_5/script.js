const btn = document.querySelector('.btn');
if (!localStorage.theme){
    localStorage.theme = 'dark'
}
document.body.className = localStorage.theme
btn.addEventListener('click', function (){
    document.body.classList.toggle('light')
    localStorage.theme = document.body.className || 'dark'
})



