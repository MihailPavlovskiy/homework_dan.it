function createNewUser(){
    const newUser = {
        firstName: prompt('Enter your name'),
        lastName: prompt('Enter your lastname'),
        birthday: prompt('Enter your birthday', 'dd.mm.yyyy'),
        getLogin: function () {
            return (this.firstName.slice(0,1) + this.lastName).toLowerCase()
        },
        getAge: function (){
            let now = new Date();
            let nowYear = now.getFullYear();
            return nowYear - this.birthday.slice(-4)

        },
        getPassword: function (){
            return (this.firstName.slice(0,1).toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(-4))

        }
    }
    return newUser
}
let user = createNewUser();
console.log(user);
console.log(user.getLogin());
console.log('User age:' + user.getAge());
console.log('Password:' + user.getPassword())


