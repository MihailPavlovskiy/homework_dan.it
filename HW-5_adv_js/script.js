const container = document.getElementById('container')
const urlUser = 'https://ajax.test-danit.com/api/json/users';
const urlPost = 'https://ajax.test-danit.com/api/json/posts';



async function getPosts (urlUser, urlPost, method){
    const [usersRes, postsRes] = await Promise.all([
        fetch(`${urlUser}`, { method }),
        fetch(`${urlPost}`, { method }),
    ]);
    const users = await usersRes.json()
    const posts = await postsRes.json()
    console.log(users)
    console.log(posts)
    for  (const post of posts) {
        const user = users.find((user) => user.id === post.userId);
        if (user) {
            const userCard = new Card(
                user.name,
                user.username,
                user.email,
                post.id,
                post.title,
                post.body
            );
          await  createPosts(userCard);
        }
    }
}


  function createPosts (userCard){
    const card =  document.createElement('div')
     const btnDelete =  document.createElement('button')
     btnDelete.innerText = 'DELETE POST'
     btnDelete.classList.add('btn-delete')
    card.classList.add('card')
    card.innerHTML = `<div> Title: ${userCard.title} </div>
    <div>Post: ${userCard.post}</div>
    <div>
    <div> Name: ${userCard.name}</div>
    <div> User_Name: ${userCard.userName}</div>
    <div> Email: ${userCard.email}</div>
    </div>`
     card.appendChild(btnDelete)
     container.append(card)
     btnDelete.addEventListener('click', async () =>{
      await deletePost(userCard.postId, card)
  } )
}


async function deletePost (id, htmlElem) {
   const res = await fetch(`https://ajax.test-danit.com/api/json/posts/${id}`, {
        method: "DELETE"
    })
    if (!res.ok){
        throw new Error('Status not 200')
    }
    await htmlElem.remove()
}

(async function () {
    await getPosts(urlUser, urlPost, 'GET');
})();
