const tabsTitle = document.querySelectorAll('.tabs-title')
const tabsContent = document.querySelectorAll('.tabs-item')

tabsTitle.forEach(function (item){
    item.addEventListener('click', function (){
        let currentTitle = item;
        let tabID =  currentTitle.getAttribute('data-tab');
        let currentContent = document.querySelector(tabID)
        tabsTitle.forEach(function (item){
            item.classList.remove('active')
        })
        tabsContent.forEach(function (item){
            item.classList.remove('active')
        })
        currentTitle.classList.add('active')
        currentContent.classList.add('active')

    })
})