const burgerMenu = document.querySelector('.header__btn-menu');
const menuList = document.querySelector('.header__menu-list');

function showMenu() {
    burgerMenu.addEventListener('click', () => {
        menuList.classList.toggle('active');
    });
}
showMenu()

