import minify from 'gulp-js-minify';
import rename from 'gulp-rename';
export const js = () => {
    return app.gulp.src(app.path.src.js, {sourcemaps: true})
        .pipe(minify())

        .pipe(rename({
            extname: ".min.js"
        }))

        .pipe(app.gulp.dest(app.path.build.js))
        .pipe(app.plugins.browsersync.stream());

}